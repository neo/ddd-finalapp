import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { ChartsModule } from 'ng2-charts'; //Load charts module
import { IonicStorageModule } from '@ionic/storage'; //Load SQlit database 

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { NutritionPage } from '../pages/nutrition/nutrition';
import { MedicationPage } from '../pages/medication/medication';
import { ProfilePage } from '../pages/profile/profile';
import { AddnutritionPage } from '../pages/addnutrition/addnutrition';
import { AddmedicationPage } from '../pages/addmedication/addmedication';
import { GuidelinesPage } from '../pages/guidelines/guidelines';
import { RemindersPage } from '../pages/reminders/reminders';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    NutritionPage,
    AddnutritionPage,
    MedicationPage,
    AddmedicationPage,
    ProfilePage,
    GuidelinesPage,
    RemindersPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    ChartsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    NutritionPage,
    AddnutritionPage,
    MedicationPage,
    AddmedicationPage,
    ProfilePage,
    GuidelinesPage,
    RemindersPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
