import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { AddmedicationPage } from '../addmedication/addmedication';


@Component({
  selector: 'page-medication',
  templateUrl: 'medication.html',
})
export class MedicationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private medStorage: Storage) {
      this.getMeds();
  }

  medDate = new Date().toISOString().substring(0, 10);
  morning ; noon ; evening ; night ;

  openMedPage() {
    this.navCtrl.push(AddmedicationPage);
  }

  getMeds() {
    this.medStorage.get('morning' + '-' + this.medDate).then(data => {
        this.morning = data ;
    });
    this.medStorage.get('noon' + '-' + this.medDate).then(data => {
        this.noon = data ;
    });
    this.medStorage.get('evening' + '-' + this.medDate).then(data => {
        this.evening = data ;
    });
    this.medStorage.get('night' + '-' + this.medDate).then(data => {
        this.night = data ;
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MedicationPage');
  }

}
