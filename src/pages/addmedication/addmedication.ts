import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-addmedication',
  templateUrl: 'addmedication.html',
})
export class AddmedicationPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private medStorage: Storage) {
  }

  medDate = new Date().toISOString();
  public medSelected ;
  public medItem ;

  public medList = [];

  addMedItem() {
    if (this.medItem) {
        this.medList.push(this.medItem);
        this.medItem = "";
    }
  }

  medSave() {
    if (this.medSelected) {
        this.medStorage.set(this.medSelected + '-' + this.medDate.substring(0, 10), this.medList);
    }
    //this.navCtrl.push(NutritionPage);
    console.log('Meds Saved...');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddmedicationPage');
  }

}
