import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-reminders',
  templateUrl: 'reminders.html',
})
export class RemindersPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  remDate = new Date().toISOString();

  public alarms = [];
  public alarmDesc;

  addAlarm() {
      this.alarms.push(this.remDate.substring(0, 10) + " " + this.remDate.substring(12, 16) + " - " + this.alarmDesc);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RemindersPage');
  }

}
