import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { NutritionPage } from '../nutrition/nutrition';

@Component({
  selector: 'page-addnutrition',
  templateUrl: 'addnutrition.html',
})
export class AddnutritionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private nutStorage: Storage) {

  }

  nutDate = new Date().toISOString();
  public mealSelected ;
  public foodItem ;
  
  public foodList = [];

  addFoodItem() {
    if (this.foodItem) {
        this.foodList.push(this.foodItem);
        this.foodItem = "";
    }
  }

  nutSave() {
    if (this.mealSelected) {
        this.nutStorage.set(this.mealSelected + '-' + this.nutDate.substring(0, 10), this.foodList);
    }
    this.navCtrl.push(NutritionPage);
    console.log('Nut Saved...');
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad AddnutritionPage');
  }

}
