import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { AddnutritionPage } from '../addnutrition/addnutrition';

@Component({
  selector: 'page-nutrition',
  templateUrl: 'nutrition.html'
})
export class NutritionPage {

  constructor(public navCtrl: NavController, private nutStorage: Storage) {
    this.getNuts();
  }

  nutDate = new Date().toISOString().substring(0, 10);
  breakfast ; lunch ; dinner ; snacks ;

  openNutPage() {
    this.navCtrl.push(AddnutritionPage);
  }

  getNuts() {
    this.nutStorage.get('breakfast' + '-' + this.nutDate).then(data => {
        this.breakfast = data ;
    });
    this.nutStorage.get('lunch' + '-' + this.nutDate).then(data => {
        this.lunch = data ;
    });
    this.nutStorage.get('dinner' + '-' + this.nutDate).then(data => {
        this.dinner = data ;
    });
    this.nutStorage.get('snacks' + '-' + this.nutDate).then(data => {
        this.snacks = data ;
    });

  }

}
